# appsamurai_aiohttp

Preliminary project implementation in aiohttp

This project was created to test the difference between Flask and Aiohttp projects.
It consists from the third and bonus assignment of the preliminary.

How to run the project:

0. Clone the project with 'git clone'
1. Import Postman collection to the Postman
2. Open the terminal
3. Run 'cd src'
4. Run 'docker-compose build'
5. Run 'docker-compose up'
6. Go to the Postman and request the 'index' to see if it works

If you do not use Postman then just go to http://0.0.0.0:5000/ 