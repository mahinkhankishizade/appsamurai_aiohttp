import logging

from aiohttp import web

from app.factory import create_app
from app.settings import config

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    app = create_app()
    web.run_app(app, port=config["app"]["port"])
