import aiohttp_cors

import app.views as view


def setup_routes(app):

    app.router.add_get("/", view.index)
    app.router.add_get("/stories/{app_token}", view.get_story_metadata)
    app.router.add_post("/event/{app_token}", view.set_story_metric)
    app.router.add_get("/activity/{app_token}", view.get_dau)

    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_methods=["POST", "PUT", "GET", "OPTIONS", "DELETE"],
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
        },
    )

    for route in app.router.routes():
        cors.add(route)
