import json

from aiohttp import web


class Response(object):

    @staticmethod
    def success(response=None, message="OK"):
        return web.json_response(
            data={
                "status_code": 200,
                "message": message,
                "response": response
            },
            status=200,
            dumps=json.dumps,
        )

    @staticmethod
    def invalid_input(message="Invalid Input", description=None):
        return web.json_response(
            data={
                "status_code": 422,
                "message": message,
                "description": description,
            },
            status=422,
        )

    @staticmethod
    def error(message="Error", description=None):
        return web.json_response(
            data={
                "status_code": 500,
                "message": message,
                "description": description,
            },
            status=500,
        )
