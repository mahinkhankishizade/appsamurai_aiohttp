from aiohttp import web

from app.routes import setup_routes
from app.settings import config


def create_app(test=False):
    app = web.Application()
    setup_routes(app)
    app["config"] = config
    app["dataport_token"] = ""

    return app
