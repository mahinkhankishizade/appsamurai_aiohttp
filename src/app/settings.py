import os
import pathlib
import yaml


BASE_DIR = pathlib.Path(__file__).parent.parent
config_file = os.getenv("CONF", "polls.test.yaml")
config_path = BASE_DIR / "app" / "config" / config_file


def get_config(path):
    with open(path) as f:
        _config = yaml.load(f, Loader=yaml.FullLoader)
    return _config


config = get_config(config_path)
