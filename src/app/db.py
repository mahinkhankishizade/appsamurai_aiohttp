import logging
import aiomysql


class Database:
    def __init__(self):
        logging.info("Connecting to database")

    async def create_pool(self, loop):
        self.pool = await aiomysql.create_pool(
            host='database',
            port=3306,
            user='root',
            password='',
            db='appsamurai',
            loop=loop,
        )

    async def fetch_one(self, sql):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql)
                result = await cur.fetchall()
                return result[0] if result else None
        self.pool.close()
        await self.pool.wait_closed()

    async def fetch_all(self, sql):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql)
                result = await cur.fetchall()
                return result if result else None
        self.pool.close()
        await self.pool.wait_closed()

    async def execute_sql(self, sql):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql)
                await conn.commit()
        self.pool.close()
        await self.pool.wait_closed()
