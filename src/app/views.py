import asyncio
import json

from datetime import datetime

from app.db import Database
from app.settings import config
from app.responses import Response

r = Response()


def index(request):
    VERSION = config["app"]["version"]
    message = f"Hello AppSamurai! aiohttp version: {VERSION}"
    return r.success(response=message)


async def get_story_metadata(request):
    """Get story metadata

    Gets
    app_token: token of an app

    Returns
    app_id: id of an app
    ts: timestamp
    metadata: list of story_ids and metadata
    story_id: id of a story
    metadata: metadata of a story
    """
    try:
        # Connect to MySQL db
        db = Database()
        loop = asyncio.get_event_loop()
        await db.create_pool(loop)

        # Fetch app_token from the request
        app_token = request.match_info["app_token"]

        # Check if the app with this token exists in the database
        if app_token:
            app = await db.fetch_one(
                f"select * from appsamurai.app where token = '{app_token}'"
            )
            if app:
                app_id, token = app
                timestamp = int(datetime.utcnow().timestamp())
                # Fetch the story metadata with the app_token
                story_metadata = await db.fetch_all(
                    f"select * from appsamurai.story_metadata \
                    where app_id = {app_id}"
                )
                # Create result
                result = {
                    "app_id": app_id,
                    "ts": timestamp,
                    "metadata": []
                }
                if story_metadata:
                    result["metadata"] = [
                        {
                            "id": story_id,
                            "metadata": json.loads(metadata)["img"]
                        } for story_id, metadata, app_id in story_metadata
                    ]
                else:
                    # If the app does not have stories return []
                    result["metadata"] = []

                return r.success(response=result)
            else:
                return r.invalid_input(description="The app token is invalid")
    except Exception as e:
        return r.error(description=e)


async def set_story_metric(request):
    """Add story event

    Event: Event is a common name for
    all types of events.

    Gets
    app_token: token of an app
    story_id: id of a story
    event_type: type of an event (impression/close etc.)
    user_id: if of a user who triggers event

    Constraints
    UNIQUE (app_id, story_id, event_type,
    user_id, event_date)

    Returns
    success: the metric was added
    error: app_token is invalid/payload is invalid
    """
    try:
        # Connect to MySQL db
        db = Database()
        loop = asyncio.get_event_loop()
        await db.create_pool(loop)

        # Fetch app_token from the request
        app_token = request.match_info["app_token"]

        # Fetch JSON from the request
        payload = await request.json()

        # Check if the payload is correct
        if (
            "story_id" in payload
            and "event_type" in payload
            and "user_id" in payload
        ):
            story_id = int(payload["story_id"])
            event_type = payload["event_type"]
            user_id = int(payload["user_id"])
        else:
            return r.invalid_input(description="The payload is invalid")

        # Check if the app with this token exists in the database
        if app_token:
            app = await db.fetch_one(
                f"select * from appsamurai.app where token = '{app_token}'"
            )
            if app:
                app_id, token = app
                # Check duplicate UNIQUE (app_id, story_id, event_type,
                # user_id, event_date)
                sql = f"SELECT id, event_count \
                    FROM appsamurai.story_metric \
                    WHERE app_id = {app_id} \
                    and story_id = {story_id} \
                    and event_type = '{event_type}' \
                    and user_id = {user_id} \
                    and date_format(event_date, '%Y-%m-%d') \
                    = date_format(NOW(), '%Y-%m-%d')"
                metric = await db.fetch_one(sql)
                if metric:
                    metric_id, e_count = metric

                    # The story_metric already exists, increase count
                    sql = f"UPDATE appsamurai.story_metric \
                        SET event_count = event_count + 1\
                        WHERE story_metric.id = {metric_id}"

                    await db.execute_sql(sql)
                else:
                    # The story_metric does not exist, insert
                    sql = f"INSERT INTO appsamurai.story_metric \
                        (app_id, story_id, event_type, user_id) VALUES \
                        ({app_id}, {story_id}, '{event_type}', {user_id})"
                    await db.execute_sql(sql)

                return r.success(response="The metric was added")
            else:
                return r.invalid_input(description="The app token is invalid")
    except Exception as e:
        return r.error(description=e)


async def get_dau(request):
    """Get daily active users (DAU)

    DAU: Total number of active users of an app
    on a specific date

    Gets
    app_token: token of an app
    date: a specific date in Y-m-d format

    Returns
    app_id: id of an app
    date: a requested date
    total: total number of users
    daus: list of user_ids in format [1, 2, 3]
    user_id: id of an active user
    """
    try:
        # Connect to MySQL db
        db = Database()
        loop = asyncio.get_event_loop()
        await db.create_pool(loop)

        # Fetch app_token from the request
        app_token = request.match_info["app_token"]

        # Date in YYYY-MM-DD format
        date = request.rel_url.query.get("date")

        # Check if the app with this token exists in the database
        if app_token:
            app = await db.fetch_one(
                f"select * from appsamurai.app where token = '{app_token}'"
            )
            if app:
                app_id, token = app
                if date:
                    sql = f"select app_id, date_format(event_date, '%Y-%m-%d') as date, \
                        count(DISTINCT user_id) as total, \
                        group_concat(distinct user_id) as daus \
                        from story_metric where \
                        date_format(event_date, '%Y-%m-%d') = '{date}' \
                        and app_id = {app_id} \
                        group by app_id"
                else:
                    return r.invalid_input(
                        description="Enter date parameter in Y-m-d format"
                    )

                result = await db.fetch_one(sql)
                final = {
                    "app_id": app_id,
                    "date": date,
                    "total": 0,
                    "daus": []
                }
                if result:
                    app_id, date, total, daus = result
                    final["total"] = total
                    final["daus"] = [int(x) for x in list(daus.split(','))]
                return r.success(response=final)
            else:
                return r.invalid_input(description="The app token is invalid")
    except Exception as e:
        return r.error(description=e)
