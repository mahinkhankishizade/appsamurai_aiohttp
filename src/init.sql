START TRANSACTION;

SET PASSWORD FOR root@localhost=PASSWORD('');
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

CREATE DATABASE IF NOT EXISTS appsamurai;

USE appsamurai;

CREATE TABLE IF NOT EXISTS appsamurai.app (
    id int NOT NULL AUTO_INCREMENT,
    token varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS appsamurai.story_metadata (
    story_id int NOT NULL AUTO_INCREMENT,
    metadata JSON NOT NULL,
    app_id int NOT NULL,
    PRIMARY KEY (story_id),
    FOREIGN KEY (app_id) REFERENCES appsamurai.app(id)

);

CREATE TABLE IF NOT EXISTS appsamurai.story_metric (
    id int NOT NULL AUTO_INCREMENT,
    story_id int NOT NULL,
    app_id int NOT NULL,
    event_date datetime DEFAULT NOW(),
    event_type varchar(255) NOT NULL,
    event_count int DEFAULT 1,
    user_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (story_id) REFERENCES appsamurai.story_metadata(story_id),
    FOREIGN KEY (app_id) REFERENCES appsamurai.app(id)
);

INSERT INTO appsamurai.app VALUES (1, 'token_1'), (2, 'token_2'), (3, 'token_3');

INSERT INTO appsamurai.story_metadata (app_id, story_id, metadata) VALUES
                                    (1, 1, '{"img": "image1.png"}'),
                                    (1, 2, '{"img": "image2.png"}'),
                                    (1, 3, '{"img": "image3.png"}'),
                                    (2, 4, '{"img": "image4.png"}'),
                                    (3, 5, '{"img": "image5.png"}'),
                                    (3, 6, '{"img": "image6.png"}'),
                                    (3, 7, '{"img": "image7.png"}'),
                                    (3, 8, '{"img": "image8.png"}');

COMMIT;