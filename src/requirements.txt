aiohttp==3.6.2
aiohttp-cors==0.7.0
async-lru==1.0.2
attrs==19.3.0
chardet==3.0.4
Click==7.0
entrypoints==0.3
idna==2.8
importlib-metadata==1.5.0
isort==4.3.21
mccabe==0.6.1
multidict==4.7.4
pathspec==0.7.0
pyrsistent==0.15.7
PyYAML==5.3
regex==2020.1.8
slacker==0.9.42
six==1.14.0
toml==0.10.0
typed-ast==1.4.1
typing-extensions==3.7.2
uvloop==0.14.0
yarl==1.4.2
zipp==3.0.0
aiomysql